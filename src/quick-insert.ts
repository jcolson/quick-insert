import {
  registerSettings,
  settings,
  getSetting,
  registerMenu,
} from "./module/settings";
import { EntitySearchItem, enabledEntityTypes } from "./module/searchLib";

import { FilterList } from "./module/filterList";
import { SheetFilters } from "./module/sheetFilters";

import { importSystemIntegration } from "./module/systemIntegration";
import { registerTinyMCEPlugin } from "./module/tinyMCEPlugin";

import { SearchApp } from "./module/searchApp";
import { QuickInsert, loadSearchIndex } from "./module/core";
import { SearchFilterCollection } from "./module/searchFilters";
import { IndexingSettings } from "./module/indexingSettings";
import { resolveAfter } from "./module/utils";

function quickInsertDisabled(): boolean {
  return !game.user.isGM && getSetting(settings.GM_ONLY);
}

// Client is currently reindexing?
let reIndexing = false;

Hooks.once("init", async function () {
  registerMenu({
    menu: "indexingSettings",
    name: "QUICKINSERT.SettingsIndexingSettings",
    label: "QUICKINSERT.SettingsIndexingSettingsLabel",
    icon: "fas fa-search",
    type: IndexingSettings,
  });
  registerMenu({
    menu: "filterMenu",
    name: "QUICKINSERT.SettingsFilterMenu",
    label: "QUICKINSERT.SettingsFilterMenuLabel",
    icon: "fas fa-filter",
    type: FilterList,
  });
  registerSettings({
    [settings.FILTERS_WORLD]: () => {
      if (quickInsertDisabled()) return;
      QuickInsert.filters.loadSave();
    },
    [settings.FILTERS_CLIENT]: () => {
      if (quickInsertDisabled()) return;
      QuickInsert.filters.loadSave();
    },
    [settings.INDEXING_DISABLED]: async () => {
      if (quickInsertDisabled()) return;
      // Active users will start reindexing in deterministic order, once per 300ms

      if (reIndexing) return;
      reIndexing = true;
      const order = [...game.users.entities]
        .filter(u => u.active)
        .map(u => u.id)
        .indexOf(game.userId);
      await resolveAfter(order * 300);
      await QuickInsert.forceIndex();
      reIndexing = false;
    },
    [settings.INDEX_GUARD_ENABLED]: val => {
      if (quickInsertDisabled()) return;
      if (val) {
        localStorage.setItem("IndexGuardEnabled", "enabled");
      } else {
        localStorage.removeItem("IndexGuardEnabled");
      }
      window.location.reload();
    },
  });
});

Hooks.once("ready", function () {
  if (quickInsertDisabled()) return;
  console.log("Quick Insert | Initializing...");

  // Ensure index guard setting is synced with local storage.
  if (getSetting(settings.INDEX_GUARD_ENABLED)) {
    if (!localStorage.getItem("IndexGuardEnabled")) {
      localStorage.setItem("IndexGuardEnabled", "enabled");
    }
  } else {
    if (localStorage.getItem("IndexGuardEnabled")) {
      localStorage.removeItem("IndexGuardEnabled");
    }
  }

  // Initialize application base
  QuickInsert.filters = new SearchFilterCollection();
  QuickInsert.app = new SearchApp();

  registerTinyMCEPlugin();

  importSystemIntegration().then(systemIntegration => {
    if (systemIntegration) {
      QuickInsert.systemIntegration = systemIntegration;
      QuickInsert.systemIntegration.init();

      if (QuickInsert.systemIntegration.defaultSheetFilters) {
        registerMenu({
          menu: "sheetFilters",
          name: "QUICKINSERT.SettingsSheetFilters",
          label: "QUICKINSERT.SettingsSheetFiltersLabel",
          icon: "fas fa-filter",
          type: SheetFilters,
        });
      }
    }
  });

  document.addEventListener("keydown", evt => {
    if (QuickInsert.matchBoundKeyEvent(evt)) {
      evt.stopPropagation();
      evt.preventDefault();
      QuickInsert.open();
    }
  });

  enabledEntityTypes().forEach(type => {
    Hooks.on(`create${type}`, entity => {
      if (!entity.visible) return;
      QuickInsert.searchLib?.addItem(EntitySearchItem.fromEntity(entity));
    });
    Hooks.on(`update${type}`, entity => {
      if (!entity.visible) {
        QuickInsert.searchLib?.removeItem(entity.uuid);
        return;
      }
      QuickInsert.searchLib?.replaceItem(EntitySearchItem.fromEntity(entity));
    });
    Hooks.on(`delete${type}`, entity => {
      QuickInsert.searchLib?.removeItem(entity.uuid);
    });
  });

  console.log("Quick Insert | Search Application ready");

  const indexDelay = getSetting(settings.AUTOMATIC_INDEXING);
  if (indexDelay != -1) {
    setTimeout(() => {
      console.log("Quick Insert | Automatic indexing initiated");
      loadSearchIndex(false);
    }, getSetting(settings.AUTOMATIC_INDEXING));
  }
});

Hooks.on("renderSceneControls", (controls, html) => {
  if (!getSetting(settings.SEARCH_BUTTON)) return;
  const searchBtn = $(
    `<li class="scene-control" title="Quick Insert" class="quick-insert-open">
          <i class="fas fa-search"></i>
      </li>`
  );
  html.append(searchBtn);
  searchBtn.on("click", () => QuickInsert.open());
});

// Exports and API usage
globalThis.QuickInsert = QuickInsert;
export { SearchContext } from "./module/contexts";
export { QuickInsert };
