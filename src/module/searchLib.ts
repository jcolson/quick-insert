import Fuse from "fuse.js";
import { settings, getSetting } from "./settings";
import { TimeoutError, withDeadline } from "./utils";

export enum EntityType {
  ACTOR = "Actor",
  ITEM = "Item",
  JOURNALENTRY = "JournalEntry",
  MACRO = "Macro",
  PLAYLIST = "Playlist",
  ROLLTABLE = "RollTable",
  SCENE = "Scene",
  USER = "User",
  FOLDER = "Folder",
}

export const IndexedEntityTypes = [
  EntityType.ACTOR,
  EntityType.ITEM,
  EntityType.JOURNALENTRY,
  EntityType.MACRO,
  // EntityType.PLAYLIST, // TODO: Play on selection? Open sidebar?
  EntityType.ROLLTABLE,
  EntityType.SCENE,
  // EntityType.USER,
];

export const EntityCollections = {
  [EntityType.ACTOR]: "actors",
  [EntityType.ITEM]: "items",
  [EntityType.JOURNALENTRY]: "journal",
  [EntityType.MACRO]: "macros",
  [EntityType.PLAYLIST]: "playlists",
  [EntityType.ROLLTABLE]: "tables",
  [EntityType.SCENE]: "scenes",
  [EntityType.USER]: "users",
};

export interface EntityAction {
  id: string;
  icon: string;
  title: string;
  hint?: string;
}

export function enabledEntityTypes(): EntityType[] {
  const disabled = getSetting(settings.INDEXING_DISABLED);
  return IndexedEntityTypes.filter(
    t =>
      //@ts-ignore
      !disabled?.entities?.[t]?.includes(game.user.role)
  );
}

export function packEnabled(pack): boolean {
  const disabled = getSetting(settings.INDEXING_DISABLED);
  // Pack entity type enabled?
  //@ts-ignore
  if (disabled?.entities?.[pack.entity]?.includes(game.user.role)) {
    return false;
  }

  // Pack enabled?
  //@ts-ignore
  if (disabled?.packs?.[pack.collection]?.includes(game.user.role)) {
    return false;
  }

  // Not hidden?
  return !(pack.private && !game.user.isGM);
}

export const entityIcons = {
  Actor: "fa-user",
  Item: "fa-suitcase",
  JournalEntry: "fa-book-open",
  Macro: "fa-terminal",
  Playlist: "fa-music",
  RollTable: "fa-th-list",
  Scene: "fa-map",
  User: "fa-users",
};

export type SearchResultPredicate = (item: SearchResult) => boolean;

export abstract class SearchItem {
  name: string;
  id: string;
  img: string;
  entityType: EntityType;
  uuid: string;

  constructor(id: string, name: string, img?: string) {
    this.id = id;
    this.name = name;
    this.img = img;
  }

  // Get the draggable attributes in order to make custom elements
  get draggableAttrs(): Record<string, string> {
    return {};
  }
  // Get the html for an icon that represents the item
  get icon(): string {
    return "";
  }
  // Get a clickable (preferrably draggable) link to the entity
  get link(): string {
    return "";
  }
  // Reference the entity in a journal, chat or other places that support it
  get journalLink(): string {
    return "";
  }
  // Reference the entity in a script
  get script(): string {
    return "";
  }
  // Short tagline that explains where/what this is
  get tagline(): string {
    return "";
  }
  // Show the sheet or equivalent of this search result
  async show(): Promise<void> {
    return;
  }

  // Fetch the original object (or null if no longer available).
  // NEVER call as part of indexing or filtering.
  // It can be slow and most calls will cause a request to the database!
  // Call it once a decision is made, do not call for every SearchItem!
  async get(): Promise<any> {
    return null;
  }
}

export class EntitySearchItem extends SearchItem {
  folder?: { id: string; name: string };

  static fromEntities(entities: Entity[]): EntitySearchItem[] {
    return entities.filter(e => e.visible).map(this.fromEntity);
  }

  static fromEntity(entity: Entity): EntitySearchItem {
    //@ts-ignore
    if (ui.PDFoundry && "pdfoundry" in entity.data.flags) {
      return new PDFoundySearchItem(entity);
    }
    return new EntitySearchItem(entity);
  }

  // Get the draggable attributes in order to make custom elements
  get draggableAttrs(): Record<string, string> {
    return {
      draggable: "true",
      "data-entity": this.entityType,
      "data-id": this.id,
    };
  }

  // Get the draggable attributes in order to make custom elements
  get draggableAttrString(): string {
    return `draggable="true" data-entity="${this.entityType}" data-id="${this.id}"`;
  }

  get icon(): string {
    return `<i class="fas ${entityIcons[this.entityType]} entity-icon"></i>`;
  }

  // Get a draggable and clickable link to the entity
  get link(): string {
    return `<a class="entity-link" ${this.draggableAttrs}>${this.icon} ${this.name}</a>`;
  }

  // Reference the entity in a journal, chat or other places that support it
  get journalLink(): string {
    return `@${this.entityType}[${this.id}]{${this.name}}`;
  }

  // Reference the entity in a script
  get script(): string {
    return `game.${EntityCollections[this.entityType]}.get("${this.id}")`;
  }

  // Short tagline that explains where/what this is
  get tagline(): string {
    if (this.folder) {
      return `${this.folder.name} - ${this.entityType}`;
    }
    return `${this.entityType}`;
  }

  async show(): Promise<void> {
    (await this.get())?.sheet.render(true);
  }

  async get(): Promise<Entity> {
    return game[EntityCollections[this.entityType]].get(this.id);
  }

  constructor(entity: Entity) {
    super(entity.id, entity.name, entity.data["img"]);

    const folder = entity.folder;
    if (folder) {
      this.folder = {
        id: folder.id,
        name: folder.name,
      };
    }

    this.entityType = (entity.entity as unknown) as EntityType;
    this.uuid = `${this.entityType}.${this.id}`;
  }
}

export class PDFoundySearchItem extends EntitySearchItem {
  get icon(): string {
    return `<img class="pdf-thumbnail" src="modules/pdfoundry/assets/pdf_icon.svg" alt="PDF Icon">`;
  }
  get journalLink(): string {
    return `@PDF[${this.name}|page=1]{${this.name}}`;
  }
  async show(): Promise<void> {
    const entity = await this.get();
    //@ts-ignore
    ui?.PDFoundry.openPDFByName(this.name, { entity });
  }
}

export class CompendiumSearchItem extends SearchItem {
  label: string;
  package: string;
  packageName: string;

  static fromCompendium(compendium: Compendium): CompendiumSearchItem[] {
    const cIndex = compendium.index as any[];
    return cIndex.map(
      (item: CompendiumIndexItem) => new CompendiumSearchItem(compendium, item)
    );
  }

  constructor(pack: Compendium, item: CompendiumIndexItem) {
    super(item.id || item._id, item.name, item.img);
    this.package = pack.collection;
    this.packageName = pack?.metadata?.label || pack.title;
    this.entityType = pack.entity as EntityType;
    this.uuid = `Compendium.${this.package}.${this.id}`;
  }

  // Get the draggable attributes in order to make custom elements
  get draggableAttrs(): Record<string, string> {
    return {
      draggable: "true",
      "data-pack": this.package,
      "data-id": this.id,
      "data-lookup": this.id,
    };
  }

  // Get the draggable attributes in order to make custom elements
  get draggableAttrsString(): string {
    // data-id is kept for pre-5.5 compat
    return `draggable="true" data-pack="${this.package}" data-id="${this.id}" data-lookup="${this.id}"`;
  }

  get icon(): string {
    return `<i class="fas ${entityIcons[this.entityType]} entity-icon"></i>`;
  }

  // Get a draggable and clickable link to the entity
  get link(): string {
    return `<a class="entity-link" ${this.draggableAttrs}>${this.icon} ${this.name}</a>`;
  }

  // Reference the entity in a journal, chat or other places that support it
  get journalLink(): string {
    return `@Compendium[${this.package}.${this.id}]{${this.name}}`;
  }

  // Reference the entity in a script
  get script(): string {
    return `fromUuid("${this.uuid}")`; // TODO: note that this is async somehow?
  }

  // Short tagline that explains where/what this is
  get tagline(): string {
    return `${this.packageName}`;
  }

  async show(): Promise<void> {
    (await this.get())?.sheet.render(true);
  }

  async get(): Promise<Entity> {
    return await fromUuid(this.uuid);
  }
}

export function isEntity(item: SearchItem): item is EntitySearchItem {
  return item instanceof EntitySearchItem;
}
export function isCompendiumEntity(
  item: SearchItem
): item is CompendiumSearchItem {
  return item instanceof CompendiumSearchItem;
}

export interface SearchResult {
  item: SearchItem;
  match;
}

class FuseSearchIndex {
  fuse: Fuse<SearchItem>;

  constructor() {
    this.fuse = new Fuse([], {
      keys: ["name"],
      includeMatches: true,
      threshold: 0.3,
    });
  }

  addAll(items: SearchItem[]) {
    for (const item of items) {
      this.fuse.add(item);
    }
  }

  add(item: SearchItem): void {
    this.fuse.add(item);
  }

  removeByUuid(uuid: string): void {
    this.fuse.remove(i => i?.uuid == uuid);
  }

  search(query: string): SearchResult[] {
    return this.fuse
      .search(query)
      .map(res => ({ item: res.item, match: res.matches }));
  }
}

export class SearchLib {
  index: FuseSearchIndex;

  constructor() {
    this.index = new FuseSearchIndex();
  }

  indexCompendium(compendium: Compendium): void {
    if (packEnabled(compendium)) {
      const index = CompendiumSearchItem.fromCompendium(compendium);
      this.index.addAll(index);
    }
  }

  async indexCompendiums(refresh = false): Promise<void> {
    for await (const res of loadIndexes(refresh)) {
      if (res.error) {
        console.log("Quick Insert | Index loading failure", res);
        continue;
      }
      console.log("Quick Insert | Index loading success", res);
      this.indexCompendium(game.packs.get(res.pack));
    }
  }

  indexDefaults(): void {
    for (const type of enabledEntityTypes()) {
      const coll = EntityCollections[type];
      this.index.addAll(EntitySearchItem.fromEntities(game[coll].entities));
    }
  }

  addItem(item: SearchItem): void {
    this.index.add(item);
  }

  removeItem(entityUuid: string): void {
    this.index.removeByUuid(entityUuid);
  }

  replaceItem(item: SearchItem): void {
    this.removeItem(item.uuid);
    this.addItem(item);
  }

  search(
    text: string,
    filter: SearchResultPredicate,
    max: number
  ): SearchResult[] {
    if (filter) {
      return this.index.search(text).filter(filter).slice(0, max);
    }
    return this.index.search(text).slice(0, max);
  }
}

export function formatMatch(
  result: SearchResult,
  formatFn: (string) => string
): string {
  const match = result.match[0];
  let text = match.value;
  [...match.indices].reverse().forEach(([start, end]) => {
    // if (start === end) return;
    text =
      text.substring(0, start) +
      formatFn(text.substring(start, end + 1)) +
      text.substring(end + 1);
  });
  return text;
}

// refresh = request new index for all compendiums
export async function* loadIndexes(
  refresh: boolean
): AsyncGenerator<any, any, any> {
  // Information about failures
  const failures: {
    [pack: string]: {
      errors: number;
      waiting?: Promise<any>;
    };
  } = {};

  const timeout = globalThis.QuickInsert.config.timeout ?? 1500;

  const packsRemaining = [];
  for (const pack of game.packs as any) {
    if (packEnabled(pack)) {
      failures[pack.collection] = { errors: 0 };
      packsRemaining.push(pack);
    }
  }

  while (packsRemaining.length > 0) {
    const pack = packsRemaining.shift();
    let promise: Promise<any>;

    // Refresh pack index
    if (refresh || pack.index.length === 0) {
      try {
        promise = failures[pack.collection].waiting ?? pack.getIndex();

        await withDeadline(
          promise,
          timeout * (failures[pack.collection].errors + 1)
        );
      } catch (error) {
        ++failures[pack.collection].errors;
        if (error instanceof TimeoutError) {
          failures[pack.collection].waiting = promise;
        } else {
          delete failures[pack.collection].waiting;
        }

        yield {
          error: error,
          pack: pack.collection,
          packsLeft: packsRemaining.length,
          errorCount: failures[pack.collection].errors,
        };
        if (failures[pack.collection].errors <= 4) {
          // Pack failed, will be retried later.
          packsRemaining.push(pack);
        } else {
          console.warn(
            `Quick Insert | Package "${pack.collection}" could not be indexed `
          );
        }
        continue;
      }
    }

    yield {
      pack: pack.collection,
      packsLeft: packsRemaining.length,
      errorCount: failures[pack.collection].errors,
    };
  }
}
