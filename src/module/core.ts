import { SearchLib, SearchResult, packEnabled } from "./searchLib";
import type { SearchApp } from "./searchApp";
import type { SearchFilterCollection } from "./searchFilters";
import type { SearchContext } from "./contexts";
import type { SystemIntegration } from "./systemIntegration";
import { getSetting, settings } from "./settings";

// Module singleton class that contains everything
class QuickInsertCore {
  // The config is not for user settings, but for programmatic config,
  // e.g. depending on the system compendium sizes
  config = {
    indexTimeout: 1500,
  };
  app: SearchApp;
  searchLib: SearchLib;
  filters: SearchFilterCollection;
  systemIntegration: SystemIntegration;

  matchBoundKeyEvent(event): boolean {
    if (this.app.embeddedMode || !event) return false;

    let keySetting = getSetting(settings.QUICKOPEN);
    // keybinds ending with space are trimmed by 0.7.x settings window
    if (keySetting.endsWith("+")) {
      keySetting = keySetting + "  ";
    }

    const key = window.Azzu.SettingsTypes.KeyBinding.parse(keySetting);
    if (key.key === " " && canvas.controls?.ruler?.waypoints?.length > 0) {
      return false;
    }

    return (
      window.Azzu.SettingsTypes.KeyBinding.eventIsForBinding(event, key) &&
      !$(document.activeElement)
        .closest(".app.window-app")
        .is("#client-settings")
    );
  }

  open(context?: SearchContext): void {
    this.app.render(true, { context });
  }

  search(text: string, filter = null, max = 100): SearchResult[] {
    return this.searchLib.search(text, filter, max);
  }

  async forceIndex(): Promise<void> {
    return loadSearchIndex(true);
  }
}

export const QuickInsert = new QuickInsertCore();

// Ensure that only one loadSearchIndex function is running at any one time.
let isLoading = false;
export async function loadSearchIndex(refresh: boolean): Promise<void> {
  if (isLoading) return;
  isLoading = true;
  console.log("Quick Insert | Preparing search index...");
  const start = performance.now();
  QuickInsert.searchLib = new SearchLib();
  QuickInsert.searchLib.indexDefaults();

  QuickInsert.filters.resetFilters();
  QuickInsert.filters.loadDefaultFilters();
  QuickInsert.filters.loadSave();

  console.log("Quick Insert | Indexing compendiums...");
  await QuickInsert.searchLib.indexCompendiums(refresh);

  console.log(
    `Quick Insert | Search index and filters completed. Indexed ${
      // @ts-ignore
      QuickInsert.searchLib?.index?.fuse._docs.length || 0
    } items in ${performance.now() - start}ms`
  );

  isLoading = false;
}
