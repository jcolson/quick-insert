import {
  CompendiumSearchItem,
  EntityAction,
  EntityCollections,
  EntitySearchItem,
  EntityType,
  IndexedEntityTypes,
  entityIcons,
} from "./searchLib";

export const ENTITYACTIONS: Record<string, (item: any) => any> = {
  show: item => item.show(),
  roll: item => item.get().then(e => e.draw()),
  viewScene: item => item.get().then(e => e.view()),
  activateScene: item =>
    item.get().then(e => {
      game.user.isGM && e.activate();
    }),
  execute: item => item.get().then(e => e.execute()),

  insert: item => item,
  rollInsert: item =>
    item.get().then(async e => {
      const roll = e.roll();
      for (const res of roll.results) {
        if (res.resultId == "") {
          return res.text;
        }

        if (res.collection.includes(".")) {
          const pack = game.packs.get(res.collection);
          const indexItem = game.packs
            .get(res.collection)
            .index.find(i => i._id === res.resultId);
          return new CompendiumSearchItem(pack, indexItem);
        } else {
          const entity = game[EntityCollections[res.collection]].get(
            res.resultId
          );
          return new EntitySearchItem(entity);
        }
      }
    }),
};

export const BrowseEntityActions: Record<string, EntityAction[]> = (() => {
  const actions = {
    [EntityType.SCENE]: [
      {
        id: "activateScene",
        icon: "fas fa-bullseye",
        title: "Activate",
      },
      {
        id: "viewScene",
        icon: "fas fa-eye",
        title: "View",
      },
      {
        id: "show",
        icon: "fas fa-cogs",
        title: "Configure",
      },
    ],
    [EntityType.ROLLTABLE]: [
      {
        id: "roll",
        icon: "fas fa-dice-d20",
        title: "Roll",
      },
      {
        id: "show",
        icon: `fas ${entityIcons[EntityType.ROLLTABLE]}`,
        title: "Edit",
      },
    ],
    [EntityType.MACRO]: [
      {
        id: "execute",
        icon: "fas fa-play",
        title: "Execute",
      },
      {
        id: "show",
        icon: `fas ${entityIcons[EntityType.ROLLTABLE]}`,
        title: "Edit",
      },
    ],
  };
  IndexedEntityTypes.forEach(type => {
    if (actions[type]) return;
    actions[type] = [
      {
        id: "show",
        icon: `fas ${entityIcons[type]}`,
        title: "Show",
      },
    ];
  });
  return actions;
})();

// Same for all inserts
const insertAction = {
  id: "insert",
  icon: `fas fa-plus`,
  title: "Insert",
};

export const InsertEntityActions: Record<EntityType, EntityAction[]> = (() => {
  const actions: Record<string, EntityAction[]> = {
    [EntityType.SCENE]: [
      {
        id: "show",
        icon: "fas fa-cogs",
        title: "Configure",
      },
    ],
    [EntityType.ROLLTABLE]: [
      {
        id: "rollInsert",
        icon: "fas fa-play",
        title: "Roll and Insert",
      },
      {
        id: "show",
        icon: `fas ${entityIcons[EntityType.ROLLTABLE]}`,
        title: "Show",
      },
    ],
  };

  // Add others
  IndexedEntityTypes.forEach(type => {
    if (!actions[type]) {
      // If nothing else, add "Show"
      actions[type] = [
        {
          id: "show",
          icon: `fas ${entityIcons[type]}`,
          title: "Show",
        },
      ];
    }
    actions[type].push(insertAction);
  });
  return actions;
})();
